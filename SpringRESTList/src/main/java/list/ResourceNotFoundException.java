package list;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="No such resource exists")
public class ResourceNotFoundException extends RuntimeException {

	public ResourceNotFoundException(){
		super();
	}
}