package list;

import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ListController {

	ArrayList<List> lists = new ArrayList<List>();
	
    @RequestMapping(method=RequestMethod.GET, value = "/lists")
    public ArrayList<List> getList(@RequestParam(value="name", defaultValue="NIL")String name) {
    	if (name.equals("NIL")){
    		return lists;
    	} else {
    		for (int i = 0; i < lists.size(); i++){
    			List testList = lists.get(i);
    			if (testList.getName().equals(name)){
    				ArrayList<List> returnList = new ArrayList<List>();
    				returnList.add(testList);
    				return returnList;
    			}
    		}
    	}
    	throw new ResourceNotFoundException();
    }
    
    @RequestMapping(method=RequestMethod.PUT, value = "/lists")
    public ResponseEntity<List> putList(@RequestBody List list){
    	for (int i = 0; i < lists.size(); i++){
    		List testList = lists.get(i);
    		if (testList.getName().equals(list.getName())){
    		
    		}
    	}
    	lists.add(list);
    	return new ResponseEntity<List>(list, HttpStatus.OK);
    }
    
    @RequestMapping(method=RequestMethod.GET, value = "/lists/{name}")
	public List getListByName(@PathVariable(value="name")String name){
		for (int i = 0; i < lists.size(); i++){
			List testList = lists.get(i);
			if (testList.getName().equals(name)){
				return testList;
			}
		}
	
		throw new ResourceNotFoundException();
    }
    
    @RequestMapping(method=RequestMethod.PUT, value = "/lists/{name}")
    public ResponseEntity<?> putListItem(@PathVariable(value="name")String name, @RequestBody ListObject object){
    	for (int i = 0; i < lists.size(); i++){
    		List testList = lists.get(i);
    		if (testList.getName().equals(name)){
    			for (int j = 0; j < testList.getListObjects().size(); j++){
    				ListObject testObject = testList.getListObjects().get(j);
    				if (testObject.equals(object)){
    					testObject.setQuantity(object.getQuantity());
    					testObject.setValue(object.getValue());
    					testObject.setDescription(object.getDescription());
    					return new ResponseEntity<ListObject>(object, HttpStatus.OK);
    				}
    			}
    			testList.getListObjects().add(object);
    			return new ResponseEntity<ListObject>(object, HttpStatus.OK);
    		}
    	}
    	return new ResponseEntity<String>(name, HttpStatus.NOT_FOUND);
    }
    
    @RequestMapping(method=RequestMethod.DELETE, value = "/lists/{name}")
	public void removeListByName(@PathVariable(value="name")String name){
		for (int i = 0; i < lists.size(); i++){
			List testList = lists.get(i);
			if (testList.getName().equals(name)){
				lists.remove(i);
				return;
			}
		}
	
		throw new ResourceNotFoundException();
    }
    
    @RequestMapping(method=RequestMethod.DELETE, value = "/lists/{name}/{objectName}")
	public void removeListObjectByName(@PathVariable(value="name")String name, @PathVariable(value="objectName")String objectName){
		for (int i = 0; i < lists.size(); i++){
			List testList = lists.get(i);
			if (testList.getName().equals(name)){
				for (int j = 0; j < testList.getListObjects().size(); j++){
					ArrayList<ListObject> testObjectList = testList.getListObjects();
					if (testObjectList.get(j).getName().equals(objectName)){
						testObjectList.remove(j);
						return;
					}
				}
			}
		}
	
		throw new ResourceNotFoundException();
    }
	
}