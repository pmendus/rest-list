package list;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ListObject {

	private String name;
	private int quantity;
	private double value;
	private String description;

	public ListObject(@JsonProperty("name")String name, @JsonProperty("quantity")int quantity, @JsonProperty("value")double value, @JsonProperty("description")String description){
		this.name = name;
		this.quantity = quantity;
		this.value = value;
		this.description = description;
	}
	
	public String getName(){
		return this.name;
	}
	public int getQuantity(){
		return this.quantity;
	}
	public double getValue(){
		return this.value;
	}
	public String getDescription(){
		return this.description;
	}
	
	public void setQuantity(int quantity){
		this.quantity = quantity;
	}
	public void setValue(double value){
		this.value = value;
	}
	public void setDescription(String description){
		this.description = description;
	}
	
	public boolean equals(ListObject testObject){
		if(this.getName().equals(testObject.getName())){
			return true;
		}
		return false;
	}
}
