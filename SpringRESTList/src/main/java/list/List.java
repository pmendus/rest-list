package list;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

public class List {

	private String name;
	private ArrayList<ListObject> listObjects;
	
	public List(@JsonProperty("name")String name){
		this.name = name;
		this.listObjects = new ArrayList<ListObject>();
	}
	
	public String getName(){
		return this.name;
	}
	public ArrayList<ListObject> getListObjects(){
		return this.listObjects;
	}
	
	public void addObject(ListObject object){
		this.listObjects.add(object);
	}
	public boolean removeObject(ListObject object){
		for(int i = 0; i < this.listObjects.size(); i++){
			ListObject testObject = this.listObjects.get(i);
			if (object.equals(testObject)){
				this.listObjects.remove(i);
				return true;
			}
		}
		return false;
	}
	
}
